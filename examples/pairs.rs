use inpt::{split::Group, Inpt};

#[derive(Inpt, Debug)]
#[inpt(regex = "(.*)->(.*)")]
pub struct Pair {
    pub start: (u32, u32),
    pub end: (u32, u32),
}

#[derive(Inpt, Debug)]
pub struct PairGroup {
    #[inpt(split = "Line")]
    pub name: String,
    pub pairs: Vec<Pair>,
}

#[inpt::main]
fn main(pairs: Vec<Group<PairGroup>>) {
    for PairGroup { name, pairs } in pairs.into_iter().map(|group| group.inner) {
        println!("== {} ==", name);
        for pair in pairs {
            println!("{pair:?}");
        }
    }
}
