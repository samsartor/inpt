#[inpt::main(regex = "(?:my name is|i am) (.+)")]
fn main<'a>(name: &'a str) {
    println!("hello {name}!")
}
