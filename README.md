# Inpt

Inpt is a derive crate for dumb type-level text parsing.

Read the [lastest documentation](https://samsartor.gitlab.io/inpt/inpt/index.html) for more information.

## Introduction
Imagine you need to chop up an annoying string and convert all the bits to useful types.
You could write that sort of code by hand using `split` and `from_str`, but the boiler-plate
of unwrapping and checking quickly looses all charm. Especially since that sort of parsing
shows up a lot in timed programming competitions like [advent of code](https://adventofcode.com).

Inpt tries to write that sort of parsing code for you, automatically splitting input strings
based on field types and an optional regex. Inpt is absolutely _not_ performant, strict, or formal.
Whenever possible, it does the obvious thing:
<div style="display: flex; flex-direction: row; align-items: stretch; gap: 1em">

```rust
#[inpt::main]
fn main(x: f32, y: f32) {
    println!("{}", x * y);
}
```

```text
$ echo '6,7' | cargo run
42
```

</div>

## Example
```rust
use inpt::{Inpt, inpt};

#[derive(Inpt)]
#[inpt(regex = r"(.)=([-\d]+)\.\.([-\d]+),?")]
struct Axis {
    name: char,
    start: i32,
    end: i32,
}

#[derive(Inpt)]
#[inpt(regex = "target area:")]
struct Target {
    #[inpt(after)]
    axes: Vec<Axis>,
}

impl Target {
    fn area(&self) -> i32 {
        self.axes.iter().map(|Axis { start, end, ..}| end - start).product()
    }
}


let target = inpt::<Target>("target area: x=119..176, y=-114..84").unwrap();
assert_eq!(target.area(), 11286);
```
