use darling::{ast::Data, util::SpannedValue, FromDeriveInput, FromField, FromVariant};
use proc_macro2::{Ident, Span, TokenStream};
use quote::{format_ident, quote, quote_spanned};
use regex_syntax::ast as regex_ast;
use std::{convert::Infallible, mem::replace};
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, parse_quote, parse_quote_spanned,
    punctuated::Punctuated,
    spanned::Spanned,
    DeriveInput, GenericParam, Index, ItemFn, Token,
};

#[proc_macro]
pub fn generate_inpt_impls(_: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Create tuple impls
    ('B'..='G')
        .map(|last| {
            let names = 'A'..=last;
            let idents: Vec<Ident> = names.clone().map(|i| format_ident!("{}", i)).collect();
            let idents = &idents;
            let is_last = (0..idents.len()).map(|i| i + 1 == idents.len());
            quote! {
                impl<'s, #(#idents: ::inpt::Inpt<'s>),*> ::inpt::Inpt<'s> for (#(#idents),*) {
                    fn step(text: &'s str, end: bool, trimmed: ::inpt::CharClass, guard: &mut ::inpt::RecursionGuard) -> ::inpt::InptStep<'s, Self> {
                        use ::inpt::ResultExt as _;

                        let mut rest = text;
                        let mut this = || Ok((#(
                            {
                                rest = #idents::default_trim(trimmed).trim(rest, #is_last && end);
                                let step = #idents::step(rest, #is_last && end, trimmed, guard);
                                rest = step.rest;
                                step.data?
                            },
                        )*));
                        ::inpt::InptStep {
                            data: this().within(text).expected::<Self>(),
                            rest,
                        }
                    }
                }
            }
        })
        .collect::<TokenStream>()
        .into()
}

#[proc_macro]
pub fn char_class(args: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let source = parse_macro_input!(args as syn::LitStr);
    match parse_char_class(&SpannedValue::new(source.value(), source.span())) {
        Ok(out) => out.into(),
        Err(err) => err.into_compile_error().into(),
    }
}

fn parse_char_class(source: &SpannedValue<String>) -> syn::Result<TokenStream> {
    use regex_syntax::hir::{Class, HirKind};

    if source.is_empty() {
        return Ok(quote! { ::inpt::CharClass(&[]) });
    }

    let regex = format!("[{}]", **source);
    let hir = match regex_syntax::Parser::new().parse(&regex) {
        Ok(hir) => hir,
        Err(err) => return Err(syn::Error::new(source.span(), err.to_string())),
    };
    let class = match hir.kind() {
        HirKind::Class(Class::Unicode(class)) => class,
        _ => panic!("did not parse as hir class"),
    };
    let tuples: TokenStream = class
        .iter()
        .map(|range| {
            let start = range.start();
            let end = range.end();
            quote! { (#start, #end), }
        })
        .collect();
    Ok(quote! { ::inpt::CharClass(&[#tuples]) })
}

struct ImplInptAs {
    _impl: Token![impl],
    generics: syn::Generics,
    ty: syn::Type,
    _as: Token![as],
    wrapper: syn::Type,
    _arrow: Token![=>],
    x: syn::Expr,
}

impl Parse for ImplInptAs {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let _impl = input.parse()?;
        let mut generics: syn::Generics = input.parse()?;
        let ty = input.parse()?;
        let _as = input.parse()?;
        let wrapper = input.parse()?;
        generics.where_clause = input.parse()?;
        let _arrow = input.parse()?;
        let x = input.parse()?;
        Ok(ImplInptAs {
            _impl,
            generics,
            ty,
            _as,
            wrapper,
            _arrow,
            x,
        })
    }
}

struct ImplInptAsArgs {
    impls: Punctuated<ImplInptAs, Token![,]>,
}

impl Parse for ImplInptAsArgs {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(ImplInptAsArgs {
            impls: Punctuated::parse_terminated(input)?,
        })
    }
}

#[proc_macro]
pub fn impl_inpt_as(args: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ImplInptAsArgs { impls } = parse_macro_input!(args as ImplInptAsArgs);
    impls
        .into_iter()
        .map(|impl_as| {
            let ImplInptAs {
                mut generics,
                ty,
                wrapper,
                x,
                ..
            } = impl_as;
            let lt = generics.lifetimes().next().cloned();
            let lt = match lt {
                Some(lt) => lt,
                None => {
                    let lt: syn::LifetimeDef = parse_quote! { 's };
                    generics
                        .params
                        .insert(0, GenericParam::Lifetime(lt.clone()));
                    lt
                }
            };
            let lt = &lt;
            let (impl_generics, _, where_clause) = generics.split_for_impl();
            quote! {
                impl #impl_generics ::inpt::Inpt<#lt> for #ty #where_clause {
                    fn step(text: &#lt str, end: bool, trimmed: ::inpt::CharClass, guard: &mut ::inpt::RecursionGuard) -> ::inpt::InptStep<#lt, Self> {
                        use ::inpt::ResultExt as _;
                        let mut step = <#wrapper as ::inpt::Inpt<#lt>>::step(text, end, trimmed, guard).map(#x);
                        step.data = step.data.replace_expected::<#wrapper, Self>();
                        step
                    }

                    fn default_trim(inherited: ::inpt::CharClass) -> ::inpt::CharClass {
                        <#wrapper as ::inpt::Inpt<#lt>>::default_trim(inherited)
                    }
                }
            }
        })
        .collect::<TokenStream>()
        .into()
}

#[derive(FromField)]
#[darling(attributes(inpt))]
struct InputField {
    ty: syn::Type,
    ident: Option<syn::Ident>,
    #[darling(default)]
    from_iter: SpannedValue<Option<SpannedValue<String>>>,
    #[darling(default)]
    from_str: SpannedValue<Option<bool>>,
    #[darling(default)]
    skip: Option<bool>,
    #[darling(default)]
    after: Option<bool>,
    #[darling(default)]
    before: Option<bool>,
    #[darling(default)]
    trim: Option<SpannedValue<String>>,
    #[darling(default)]
    split: Option<SpannedValue<String>>,
    #[darling(skip)]
    index: usize,
}

impl InputField {
    fn skip(&self) -> bool {
        self.skip.unwrap_or(false)
    }

    fn from_str(&self) -> bool {
        self.from_str.unwrap_or(false)
    }

    fn after(&self) -> bool {
        !self.skip() && self.after.unwrap_or(false)
    }

    fn before(&self) -> bool {
        !self.skip() && self.before.unwrap_or(false)
    }

    fn capture(&self) -> bool {
        !(self.skip() || self.after() || self.before())
    }
}

#[derive(FromVariant)]
#[darling(attributes(inpt))]
struct InputVariant {
    ident: syn::Ident,
    fields: darling::ast::Fields<SpannedValue<InputField>>,
    #[darling(default)]
    regex: Option<SpannedValue<String>>,
}

#[derive(FromDeriveInput)]
#[darling(attributes(inpt))]
struct InputOpts {
    ident: syn::Ident,
    generics: syn::Generics,
    data: Data<InputVariant, SpannedValue<InputField>>,
    #[darling(default)]
    regex: Option<SpannedValue<String>>,
    #[darling(default)]
    bounds: Option<syn::LitStr>,
    #[darling(default)]
    trim: Option<SpannedValue<String>>,
    #[darling(default)]
    from: Option<syn::LitStr>,
    #[darling(default)]
    try_from: Option<syn::LitStr>,
}

fn from_str_field(ty: &syn::Type, input: Ident) -> TokenStream {
    quote! {
        match <#ty as ::std::str::FromStr>::from_str(#input) {
            Ok(val) => Ok(val),
            Err(e) => Err(::inpt::InptError::expected_from_str::<#ty>(#input))
                .message_inside(e.to_string())
        }
    }
}

fn parse_type(ty: &SpannedValue<String>) -> syn::Result<syn::Type> {
    match syn::parse_str::<syn::Type>(ty) {
        Ok(ty) => Ok(ty),
        Err(e) => {
            let mut err = syn::Error::new(ty.span(), "could not parse type");
            err.combine(e);
            Err(err)
        }
    }
}

fn from_iter_field(
    ty: &syn::Type,
    item: &syn::Type,
    item_span: Span,
    input: Ident,
    trimmed: Ident,
) -> syn::Result<TokenStream> {
    let item = quote_spanned! { item_span => #item };
    Ok(quote_spanned! { item_span => {
        let mut iter = ::inpt::InptIter::new(#input, #trimmed);
        let data = <#ty as ::std::iter::FromIterator<#item>>::from_iter(&mut iter);
        iter.outcome.map(|_| data)
    } })
}

type Predicates = syn::punctuated::Punctuated<syn::WherePredicate, Token![,]>;

fn type_as_option(ty: &syn::Type) -> Option<&syn::Type> {
    let path = match ty {
        syn::Type::Path(p) => p,
        _ => return None,
    };
    if path.qself.is_some() {
        return None;
    }
    if path.path.leading_colon.is_some() {
        return None;
    }
    if path.path.segments.len() != 1 {
        return None;
    }
    let segment = &path.path.segments[0];
    if segment.ident != "Option" {
        return None;
    }
    let args = match &segment.arguments {
        syn::PathArguments::AngleBracketed(a) => a,
        _ => return None,
    };
    if args.args.len() != 1 {
        return None;
    }
    match &args.args[0] {
        syn::GenericArgument::Type(ty) => Some(ty),
        _ => None,
    }
}

fn regex_definition<'a>(
    regex: &str,
    fields: impl IntoIterator<Item = &'a InputField>,
    span: Span,
) -> Result<(TokenStream, TokenStream), syn::Error> {
    struct CountCaptures(pub u32);

    impl regex_ast::Visitor for CountCaptures {
        type Output = u32;
        type Err = Infallible;

        fn visit_pre(&mut self, ast: &regex_ast::Ast) -> Result<(), Infallible> {
            if let regex_ast::Ast::Group(group) = ast {
                if let regex_ast::GroupKind::CaptureIndex(idx) = group.kind {
                    self.0 = self.0.max(idx);
                }
            }
            Ok(())
        }

        fn finish(self) -> Result<u32, Infallible> {
            Ok(self.0)
        }
    }

    let ast = match regex_ast::parse::Parser::new().parse(regex) {
        Ok(ast) => ast,
        Err(err) => return Err(syn::Error::new(span, err.to_string())),
    };
    let captures = regex_ast::visit(&ast, CountCaptures(0)).unwrap();
    let fields = fields.into_iter().filter(|field| field.capture()).count();
    if captures as usize != fields {
        return Err(syn::Error::new(
            span,
            format!("expected {fields} capture groups for struture fields, found {captures}"),
        ));
    }
    let start_regex = format!("^(?:{})", regex);
    let end_regex = format!("^(?:{})$", regex);
    Ok((
        quote! { ::inpt::LazyRegex::new(#start_regex) },
        quote! { ::inpt::LazyRegex::new(#end_regex) },
    ))
}

fn sequential_field(
    f: &SpannedValue<InputField>,
    lt: &syn::LifetimeDef,
    preds: &mut Predicates,
    prefix: &str,
    is_last: bool,
) -> Result<TokenStream, syn::Error> {
    let ty = &f.ty;
    let name = format_ident!("{}_{}", prefix, f.index);
    let trimmed_name = format_ident!("{}_{}_trim", prefix, f.index);
    let mut trim_init = quote! { let #trimmed_name = trimmed; };
    let expr = if f.skip == Some(true) {
        quote! { <#ty as ::std::default::Default>::default() }
    } else if let Some(split) = &f.split {
        let mut split_ty = parse_type(split)?;
        split_ty = parse_quote! { ::inpt::split::#split_ty<&'s str> };
        trim_init =
            quote! { let #trimmed_name = <#split_ty as ::inpt::Inpt>::default_trim(trimmed); };
        let (inner, is_opt) = regex_field(f, lt, preds, prefix)?;
        if is_opt {
            return Err(syn::Error::new(
                split.span(),
                "splitting can not produce None",
            ));
        }
        quote! { {
            let step =  <#split_ty as ::inpt::Inpt>::step(rest, #is_last && end, #trimmed_name, guard);
            rest = step.rest;
            step.data.map(|split| split.inner).and_then(|full_group| #inner)
        } }
    } else if f.from_str() {
        if !is_last {
            return Err(syn::Error::new(
                f.from_str.span(),
                "without a regex or split, from_str can only be used on the last field",
            ));
        }
        from_str_field(ty, format_ident!("rest"))
    } else if let Some(item) = &*f.from_iter {
        let item_ty = parse_type(item)?;
        trim_init =
            quote! { let #trimmed_name = <#item_ty as ::inpt::Inpt>::default_trim(trimmed); };
        if !is_last {
            return Err(syn::Error::new(
                f.from_iter.span(),
                "without a regex, from_iter can only be used on the last field",
            ));
        }
        from_iter_field(
            ty,
            &item_ty,
            item.span(),
            format_ident!("rest"),
            trimmed_name.clone(),
        )?
    } else {
        trim_init = quote! { let #trimmed_name = <#ty as ::inpt::Inpt>::default_trim(trimmed); };
        preds.push(parse_quote_spanned! { f.ty.span() => #ty: ::inpt::Inpt<#lt> });
        quote! { {
            let step =  <#ty as ::inpt::Inpt>::step(rest, #is_last && end, #trimmed_name, guard);
            rest = step.rest;
            step.data
        } }
    };
    if let Some(source) = &f.trim {
        let expr = parse_char_class(source)?;
        trim_init = quote! { let #trimmed_name = #expr; };
    }
    Ok(quote_spanned! { f.span() =>
        #trim_init
        let after_trim = #trimmed_name.trim(rest, #is_last && end);
        rest = after_trim;
        #name = #expr?;
    })
}

fn regex_field(
    f: &SpannedValue<InputField>,
    lt: &syn::LifetimeDef,
    preds: &mut Predicates,
    prefix: &str,
) -> Result<(TokenStream, bool), syn::Error> {
    let trimmed_name = format_ident!("{}_{}_trim", prefix, f.index);
    let mut trim_init = quote! { let #trimmed_name = trimmed; };
    let (ty, is_opt) = match type_as_option(&f.ty) {
        Some(ty) => (ty, true),
        None => (&f.ty, false),
    };
    let trimmed_name = format_ident!("{}_{}_trim", prefix, f.index);
    let expr = if f.from_str() {
        from_str_field(ty, format_ident!("group"))
    } else if let Some(item) = &*f.from_iter {
        let item_ty = parse_type(item)?;
        trim_init =
            quote! { let #trimmed_name = <#item_ty as ::inpt::Inpt>::default_trim(trimmed); };
        from_iter_field(
            ty,
            &item_ty,
            item.span(),
            format_ident!("group"),
            trimmed_name.clone(),
        )?
    } else {
        trim_init = quote! { let #trimmed_name = <#ty as ::inpt::Inpt>::default_trim(trimmed); };
        preds.push(parse_quote_spanned! { ty.span() => #ty: ::inpt::Inpt<#lt> });
        quote! {
            <#ty as ::inpt::Inpt>::step(group, true, trimmed, guard).data
        }
    };

    if let Some(source) = &f.trim {
        let expr = parse_char_class(source)?;
        trim_init = quote! { let #trimmed_name = #expr; };
    }

    Ok((
        quote! { {
            #trim_init
            let group = #trimmed_name.trim(full_group, true);
            #expr.within(full_group)
        } },
        is_opt,
    ))
}

fn field_exprs(
    fields: &darling::ast::Fields<SpannedValue<InputField>>,
    lt: &syn::LifetimeDef,
    preds: &mut Predicates,
    prefix: &str,
    regex: Option<&SpannedValue<String>>,
) -> Result<TokenStream, syn::Error> {
    let mut this = TokenStream::new();

    for f in fields.iter() {
        match (regex.is_some(), f.before(), f.after()) {
            (false, true, _) | (false, _, true) => {
                return Err(syn::Error::new(
                    f.span(),
                    "before and after only apply when matching a regex",
                ))
            }
            (_, true, true) => {
                return Err(syn::Error::new(
                    f.span(),
                    "a field can not be marked before and after",
                ));
            }
            _ => (),
        }
    }

    for f in fields.iter().filter(|f| f.before()) {
        this.extend(sequential_field(f, lt, preds, prefix, false)?);
    }

    if let Some(regex) = regex {
        let (start_regex, end_regex) =
            regex_definition(&**regex, fields.iter().map(|f| &**f), regex.span())?;
        let regex = &**regex;
        let no_after = fields.iter().all(|f| !f.after());
        this.extend(quote! {
            static RE_SOURCE: &str = #regex;
            static RE_START: ::inpt::LazyRegex = #start_regex;
            static RE_END: ::inpt::LazyRegex = #end_regex;
            rest = trimmed.trim(rest, #no_after && end);
            let regex = if #no_after && end { &RE_END } else { &RE_START};
            let regex_text = rest;
            let capture = regex
                .captures(regex_text)
                .ok_or_else(|| ::inpt::InptError::expected_regex(RE_SOURCE, text))?;
            matched = true;
            rest = &rest[capture.get(0).unwrap().end()..];
        });

        let mut group_idx: usize = 1;
        for f in fields.iter().filter(|f| f.capture()) {
            if let Some(split) = &f.split {
                return Err(syn::Error::new(
                    split.span(),
                    "don't use split with a capture group",
                ));
            }
            let name = format_ident!("{}_{}", prefix, f.index);
            let (expr, is_opt) = regex_field(f, lt, preds, prefix)?;
            if is_opt {
                this.extend(quote_spanned! { f.span() =>
                    #name = match capture.get(#group_idx).map(|m| m.as_str()){
                        None => None,
                        Some(full_group) => Some(#expr.regex_group(#group_idx)
                            .within(regex_text)
                            .regex(RE_SOURCE)?)
                    };
                });
            } else {
                this.extend(quote_spanned! { f.span() =>
                    #name = match capture.get(#group_idx).map(|m| m.as_str()) {
                        None => return Err(::inpt::InptError::expected_regex_group(#group_idx))
                            .within(regex_text)
                            .regex(RE_SOURCE),
                        Some(full_group) => #expr.regex_group(#group_idx)
                            .within(regex_text)
                            .regex(RE_SOURCE)?,
                    };
                });
            }
            group_idx += 1;
        }
    }

    let last_index = fields
        .iter()
        .rposition(|f| {
            if regex.is_some() {
                f.after()
            } else {
                !f.skip()
            }
        })
        .unwrap_or(fields.len());
    for f in fields
        .iter()
        .filter(|f| !f.skip() && (regex.is_none() || f.after()))
    {
        this.extend(sequential_field(
            f,
            lt,
            preds,
            prefix,
            f.index == last_index,
        )?);
    }

    this.extend(quote! { matched = true; });

    for f in fields.iter().filter(|f| f.skip()) {
        let ty = &f.ty;
        let name = format_ident!("{}_{}", prefix, f.index);
        this.extend(quote! { #name = <#ty as ::std::default::Default>::default(); })
    }

    Ok(this)
}

fn field_setup(
    fields: &darling::ast::Fields<SpannedValue<InputField>>,
    prefix: &str,
) -> (TokenStream, TokenStream) {
    let mut head = TokenStream::new();
    let mut inst = TokenStream::new();
    for f in fields.iter() {
        let ty = &f.ty;
        let name = format_ident!("{}_{}", prefix, f.index);
        head.extend(quote! { let mut #name: #ty; });
        if let Some(ident) = &f.ident {
            inst.extend(quote! { #ident: #name, });
        } else {
            let idx = Index {
                index: f.index as _,
                span: Span::call_site(),
            };
            inst.extend(quote! { #idx: #name, });
        }
    }
    (head, inst)
}

fn impl_inpt_derive(
    InputOpts {
        ident,
        generics,
        data,
        regex,
        bounds,
        trim,
        from,
        try_from,
    }: InputOpts,
) -> Result<TokenStream, syn::Error> {
    let mut generics_with_lt = generics.clone();
    let lt = match generics.lifetimes().next() {
        Some(lt) => lt.clone(),
        None => {
            let lt: syn::LifetimeDef = parse_quote! { 's };
            generics_with_lt
                .params
                .insert(0, GenericParam::Lifetime(lt.clone()));
            lt
        }
    };
    let lt = &lt;

    let default_trim = if let Some(source) = &trim {
        let expr = parse_char_class(source)?;
        quote! {
            fn default_trim(_inherited: ::inpt::CharClass) -> ::inpt::CharClass {
                #expr
            }
        }
    } else if let Some(from) = from.as_ref().or(try_from.as_ref()) {
        let from = from.parse::<syn::Type>()?;
        quote! {
            fn default_trim(inherited: ::inpt::CharClass) -> ::inpt::CharClass {
                <#from as inpt::Inpt<#lt>>::default_trim(inherited)
            }
        }
    } else {
        TokenStream::new()
    };

    if from.is_some() || try_from.is_some() {
        if let Some(regex) = &regex {
            return Err(syn::Error::new(
                regex.span(),
                "don't use from or try_from with a regex",
            ));
        }
    }

    let preds = &mut generics_with_lt
        .where_clause
        .get_or_insert_with(|| parse_quote! { where })
        .predicates;
    preds.push(parse_quote! { Self: #lt });

    let mut head = TokenStream::new();
    let mut body = TokenStream::new();

    match (from, try_from, data) {
        (Some(from), _, _) => {
            let from = from.parse::<syn::Type>()?;
            preds.push(parse_quote! { #from: Inpt<#lt> });
            body.extend(quote! {
                let mut res = <#from as ::inpt::Inpt<#lt>>::step(text, end, trimmed, guard)
                    .map(From::from);
                rest = res.rest;
                res.data
            });
        }
        (_, Some(from), _) => {
            let from = from.parse::<syn::Type>()?;
            preds.push(parse_quote! { #from: Inpt<#lt> });
            body.extend(quote! {
                let mut res = <#from as ::inpt::Inpt<#lt>>::step(text, end, trimmed, guard)
                    .try_map(core::convert::TryFrom::try_from);
                rest = res.rest;
                res.data
            });
        }
        (_, _, Data::Struct(fields)) => {
            let (struct_head, inst) = field_setup(&fields, "field");
            head.extend(struct_head);
            let inner = field_exprs(&fields, &lt, preds, "field", regex.as_ref())?;
            body.extend(quote! {
                #inner
                Ok(Self {
                    #inst
                })
            });
        }
        (_, _, Data::Enum(varis)) => {
            for (idx, vari) in varis.into_iter().enumerate() {
                let prefix = format!("field_{}", idx);
                let (vari_head, inst) = field_setup(&vari.fields, &prefix);
                // head.extend(vari_head);
                let inner = field_exprs(&vari.fields, &lt, preds, &prefix, vari.regex.as_ref())?;
                let vari_ident = &vari.ident;
                body.extend(quote! {
                    rest = text;
                    let mut vari = || {
                        #vari_head
                        #inner
                        Ok(Self::#vari_ident {
                            #inst
                        })
                    };
                    let res = vari();
                    if matched {
                        return res;
                    }
                });
            }
            body.extend(quote! {
                if let Err(err) = res {
                    Err(err).expected::<Self>()
                } else {
                   Err(::inpt::InptError::expected::<Self>(text))
                }
            });
        }
    }

    if let Some(bounds) = bounds {
        struct Bounds {
            preds: Predicates,
        }

        impl Parse for Bounds {
            fn parse(input: ParseStream) -> syn::Result<Self> {
                Ok(Self {
                    preds: Punctuated::parse_terminated(input)?,
                })
            }
        }

        preds.clear();
        *preds = bounds.parse::<Bounds>()?.preds;
    }

    let (impl_generics, _, where_clause) = generics_with_lt.split_for_impl();
    let (_, ty_generics, _) = generics.split_for_impl();

    Ok(quote! {
        impl #impl_generics ::inpt::Inpt<#lt> for #ident #ty_generics #where_clause {
            fn step(text: &#lt str, end: bool, trimmed: ::inpt::CharClass, guard: &mut ::inpt::RecursionGuard) -> ::inpt::InptStep<#lt, Self> {
                use ::inpt::ResultExt as _;

                guard.check(text, |guard| {
                    let mut rest = text;
                    let mut matched = false;
                    let mut this = || { #head #body };
                    ::inpt::InptStep {
                        data: this().within(text).expected::<Self>(),
                        rest,
                    }
                })
            }

            #default_trim
        }
    })
}

#[proc_macro_derive(Inpt, attributes(inpt))]
pub fn inpt_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let mut input = match InputOpts::from_derive_input(&input) {
        Ok(opts) => opts,
        Err(err) => return err.write_errors().into(),
    };
    match &mut input.data {
        Data::Enum(e) => {
            for v in e {
                for (idx, f) in v.fields.fields.iter_mut().enumerate() {
                    f.index = idx;
                }
            }
        }
        Data::Struct(s) => {
            for (idx, f) in s.fields.iter_mut().enumerate() {
                f.index = idx;
            }
        }
    }
    match impl_inpt_derive(input) {
        Ok(out) => out.into(),
        Err(err) => err.into_compile_error().into(),
    }
}

fn impl_main_attribute(args: TokenStream, mut item: ItemFn) -> Result<TokenStream, syn::Error> {
    let mut field_defs = Vec::new();
    let mut field_pats = Vec::new();
    for (index, arg) in item.sig.inputs.iter_mut().enumerate() {
        if let syn::FnArg::Typed(arg) = arg {
            let name = format_ident!("arg{}", index);
            let ty = &arg.ty;
            let pat = &arg.pat;
            let (inpt_attrs, other_attrs): (Vec<_>, Vec<_>) = arg
                .attrs
                .drain(..)
                .partition(|attr| attr.path.is_ident("inpt"));
            arg.attrs.extend(other_attrs.into_iter());
            field_defs.push(quote! { #(#inpt_attrs)* #name: #ty });
            field_pats.push(quote! { #name: #pat });
        }
    }
    let id = &item.sig.ident;
    let gen = &item.sig.generics;
    let output = &item.sig.output;
    let vis = &item.vis;
    let body = &item.block;
    let attrs = replace(&mut item.attrs, Default::default());

    Ok(quote! {
        #(#attrs)*
        #vis fn #id() #output {
            #[derive(::inpt::Inpt)]
            #[inpt(#args)]
            struct Arguments #gen {
                #(#field_defs,)*
            }

            let Arguments { #(#field_pats,)* } = ::inpt::inpt_stdio();
            #body
        }
    })
}

#[proc_macro_attribute]
pub fn main(
    args: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let item = parse_macro_input!(item as ItemFn);
    match impl_main_attribute(args.into(), item) {
        Ok(out) => out.into(),
        Err(err) => err.into_compile_error().into(),
    }
}
