use inpt::{inpt, Inpt};

#[test]
fn test_seq_vec() {
    assert_eq!(inpt::<Vec<i32>>("1 2 3 4").unwrap(), vec![1, 2, 3, 4])
}

#[test]
fn test_seq_array() {
    assert_eq!(inpt::<[i32; 4]>("1 2 3 4").unwrap(), [1, 2, 3, 4])
}

#[test]
fn test_char_array() {
    assert_eq!(inpt::<[char; 4]>("ohea").unwrap(), ['o', 'h', 'e', 'a'])
}

#[test]
fn test_seq_array_commas() {
    assert_eq!(inpt::<[i32; 4]>(",1, 2,3 ,4, ").unwrap(), [1, 2, 3, 4])
}

#[test]
fn test_seq_vec_commas() {
    assert_eq!(inpt::<Vec<i32>>(",1, 2,3 ,4, ").unwrap(), vec![1, 2, 3, 4])
}

#[test]
fn test_char_tuple() {
    assert_eq!(
        inpt::<(char, char, char, char)>("ohea").unwrap(),
        ('o', 'h', 'e', 'a')
    )
}

#[test]
fn test_seq_tuple() {
    assert_eq!(
        inpt::<(i32, i32, i32, i32)>("1 2 3 4").unwrap(),
        (1, 2, 3, 4)
    )
}

#[test]
fn test_seq_tuple_commas() {
    assert_eq!(
        inpt::<(i32, i32, i32, i32)>(",1, 2,3 ,4, ").unwrap(),
        (1, 2, 3, 4)
    )
}

#[test]
fn test_char_array_end() {
    assert_eq!(
        inpt::<[char; 4]>("oheartns")
            .unwrap_err()
            .unparsable_text()
            .unwrap(),
        "artns"
    )
}

#[test]
fn test_char_tuple_end() {
    assert_eq!(
        inpt::<(char, char, char, char)>("oheartns")
            .unwrap_err()
            .unparsable_text()
            .unwrap(),
        "artns"
    )
}

#[test]
fn test_seq_derive() {
    #[derive(Inpt)]
    struct SeqWrapper(#[inpt(from_iter = "i32")] Vec<i32>);

    assert_eq!(inpt::<SeqWrapper>("1 2 3 4").unwrap().0, vec![1, 2, 3, 4])
}
